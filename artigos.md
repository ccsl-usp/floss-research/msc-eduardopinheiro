### Too many lords, not enough stewards

* Pretty much every subsystem has their "local toxic person" that cannot be removed because of their accumulated store of knowledge
* the **group maintainership model** was pushed at Kernel Summits, but it has only been adopted in a few places: the x86 subsystem, ARM at the top level, and half of the graphics subsystem.
*  rules are "outright not documented"
* knowledge, tools and tests are not being shared among members of the community instead maintainers keep it to themself making them unreplaceble in the project and therefore with power
* Change is hard because even maintainers that do think there shoud be some are scared they will be public shamed if something went wrong with the changes like **group maintainership**, this fear also leads maintainers to have a more controlling aproach do the development.

### Group maintainership models

* load balacing to of work
* prevents problems with personal availability
* great to develop new maintainers

#### Two models for group maintainership
* **hands-off** - manage a single repository using a IRC channel to "lock" it when changes are ready to be applyed, they also keep a log with the changes made
* **delegation** - subsystems that use patchwork, it can delegate the handling of each patch to a specific maintainer (media subsystem,  power-management subsystem)

##### multiple-committer model 
* used in the i915 graphics driver subsystem
* many committers have the ability to commit changes to the repository.
* commiter vs maintainer: committers work internally, while the maintainer deals with the rest of the world
* maintainer job in this model is to accept the blame when things go wrong

### On moving on from being a maintainer
*  no clear path for relinquishing the maintainer role—and generally no succession plan

* Brown's announcement describes the responsibilities of a maintainer as he sees them:

    ** to gather and manage patches and outstanding issues,
    ** to review patches or get them reviewed
    ** to follow up bug reports and get them resolved
    ** to feed patches upstream, maybe directly to Linus, maybe through some other maintainer, depending on what relationships already exist or can be formed,
    ** to guide the longer term direction (saying "no" is important sometimes),
    ** to care,

    but also to be aware that maintainership takes real effort and time, as does anything that is really worthwhile.

* creating a vacuum as an exit strategy may actually work better than other mechanisms—at least for some subsystems and maintainers

### Linux Kernel Maintainer Statistics

*  the people in power don’t bother to apply the same rules to themselves as anyone else
* **This is relatively easy to measure accurately in git: If the recorded patch author and committer match, it’s a maintainer self-commit, if they don’t match it’s a contributor commit.**
* Maintainer self-commits have a higher change of not being reviewed
* **group maintainership model** higher number of self-commits but with a high percentage of them being reviewed (a better change in training new maintainers)
* Maintainers are not keeping up with the kernel growth overall
* Naively extrapolating the relative trend predicts that around the year 2025 large numbers of kernel maintainers will do nothing else than be the bottleneck

### Maintainers don’t escale

* How a chnage is applyed to the Kernel: Changes are submitted as patches to mailing list, then get some review and eventually get applied by a maintainer to that maintainer’s git tree. Each maintainer then sends pull request, often directly to Linus.
* subsystems with group maintainership models of different kinds: TIP (x86 and core kernel), ARM-SoC and the graphics subsystem (DRM).
* The current maintainership model is very flawed and tends to not scale very well.


### The Maintainer’s Paradox: Balancing Project and Community

* The Maintainer’s Paradox is that the maintainer is really excited about new contributions, but there is also fear and trepidation. Sometimes when I see a patch set on the mailing list I say, ‘Oh no, another patch set.’ I just might not have time to look at it. You want to review patches carefully and give appropriate feedback, but being a maintainer is sometimes overwhelming.

### The code of conduct at the Maintainers Summit

* Introducing changes is quite hard in the kernel community, mostly because of its size
* Chnaging the maintainer’s culture is proving to be a challange that will be around in the kernel for a long time and may be define if the project will keep growing or start failing ion the future.

### Two perspectives on the maintainer relationship

* Interesting view on both the developer and maintainer responsibilities
* Communication is key, helping each other to make it work

### Why kernel development still uses email

* The kernel is able to process a huge amount of patches each day, so the current system "is working"
* Github PR seems to not be able to scale well, as the Kubernetes project huge amount of open issues and PRs indicates
* Email has been around forever, and everybody has access to it in one form or another. There are plenty of free email providers and a vast number of clients. Email works well for non-native speakers, who can use automatic translation systems if need be. Email is also friendly from an accessibility standpoint; that has helped the kernel to gain a number of very good blind developers. Email is fast, it makes local testing easy, and remote testing is possible. Writing scripts to deal with emailed patches is easily done. And there is no need to learn a new interface to work with it.
* Poor arguments in opnion

### On saying "no"

* Patches are not being reviewed properly before getting accept
* Maybe group maintainership could improve this situation with more people being able to review code

### On the scalability of Linus

### Patch flow into the mainline for 4.14

